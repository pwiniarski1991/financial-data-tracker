import React from 'react';
import { Link, Route } from 'react-router-dom' 
import { Sh1, Sheader, Sli, Smain, Snav, Sul } from './StyledComponents' 
import Home from './pages/Home'
import AddForm from './pages/AddForm'
import useFetcher from './hooks/useFetcher'
import './App.css';


function App() {
  const { error, isLoading, resource, setKeywords } = useFetcher();

  if(error.length) return <p>{error}</p>
  if(!resource && isLoading) return <div>Loading...</div>

  return (
    <div className="App">
      <Sheader>
        <Sh1>Stock Tracker</Sh1>
        <Snav>
          <Sul display='flex'>
            <Sli key={1}>
              <Link to="/add">Track new company</Link>
            </Sli>
            <Sli key={2}>
              <Link to="/">Companies</Link>
            </Sli>
          </Sul>
        </Snav>
      </Sheader>
      <Smain>
        <Route path="/" exact render={props => <Home {...props} data={resource} />} />
        <Route path="/add" component={props => <AddForm {...props} setKeywords={setKeywords} />} />
      </Smain>
    </div>
  );
}

export default App;
