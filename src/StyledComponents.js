import styled from 'styled-components'

const Sbutton = styled.button`
    display: block;
    text-decoration: none;
    color: #ffffff;
    border-color: transparent;
    outline-style: none;
    background-color: rgba(0, 153, 255, 1);
    border: none;
    border-radius: 2px;
    margin-top: 20px;
    padding: 10px 20px;
`

const Sdiv = styled.div`
    display: flex;
    flex-direction: ${props => props.direction || 'row'};
    margin-left: 10px;
    flex-wrap: wrap;
`

const Sform = styled.form`
    margin: 10px 0;
`

const Sheader = styled.header`
    display: flex;
    margin: 0 20px;
    background-color: #F2F2F2;
    padding: 10px 25px;
    border: 1px solid #CCCCCC;
    border-radius: 4px;
`

const Sh1 = styled.h1`
    margin: 0;
    color: #666666;
`

const Sh2 = styled.h2`
    margin: 0;
`

const Sh4 = styled.h4`
    margin: 0 20px 0 0;
`

const Slabel = styled.label`
    display: block;
    
`

const Snav = styled.nav`
    display: flex;
`

const Sul = styled.ul`
    display: ${props => props.display || 'block' };
    list-style-type: none;
    padding-left: 0;
`

const Sli = styled.li`
    display: ${props => props.display || 'inherit' };
    margin: ${props => props.margin || '0 20px'};
    align-items: center;
    a {
        color: #0099FF;
    }
`

const Simg = styled.img`
    display: block;
    width: 64px;
    height: 64px;
`

const Sinput = styled.input`
    margin: 20px 0;
    padding: 10px 20px 10px 10px;
    border: 1px solid black;
    color: #CCCCCC;
    border-color: #CCCCCC;
    border-radius: 4px;
`

const Smain = styled.main`
    display: flex;
    margin: 20px 0 0 20px;
    flex-direction: ${props => props.direction || 'column'};
`

export { Sbutton, Sdiv, Sform, Sh1, Sh2, Sh4, Sheader, Simg, Sinput, Slabel, Sli, Smain, Snav, Sul }