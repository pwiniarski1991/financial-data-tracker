import React from 'react'
import { ErrorMessage, Formik } from 'formik'
import { Sbutton, Sform, Sinput, Slabel } from './../StyledComponents'


function Form(props) {
    
    function handleSubmit({ company }) {
        const val = company.split(/[\s,]/).join(', ');
        props.setKeywords(val);
        props.history.push('/');
    }

    return (
        <Formik
            initialValues={{ company: '' }}
            validate={values => {
                const errors = {};
                if (!values.company) errors.company = 'Provide the stock exchange symbol of a company you want to track';
                return errors;
            }}
            onSubmit={handleSubmit}
        >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <Sform onSubmit={handleSubmit}>
          <Slabel htmlFor="company">Company Symbol</Slabel>
          <Sinput
            id="company"
            name="company"
            placeholder="Company symbol"
            onChange={handleChange}
            onBlur={handleBlur}
          />
          {errors.company && touched.company 
            && <ErrorMessage name="company" component="div" />}
          <Sbutton type="submit" disabled={isSubmitting}>
            Track
          </Sbutton>
        </Sform>
      )}
    </Formik>
    )
}

export default Form