import React from 'react'
import PropTypes from 'prop-types'
import { Sul } from './../StyledComponents'

function List(props) {

    return (
        <Sul>
            {props.children}
        </Sul>
    )
}

List.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.shape({
          type: 'li'
        })
      ])
}

List.defaultProps = {
    children: ['item1', 'item2'].map(el => <li>{ el }</li>)
}

export default List