import React from 'react'
import { Sdiv, Sh4, Simg, Sli } from './../StyledComponents'

function ListItem({ item }) {
    const el = Object.keys(item).reduce((acc, curr) => {
         acc[curr.split('.')[1].trim()] = item[curr];
         return acc;
    }, {});
    return(
        <Sli display="flex" margin="10px 0" >
            <Simg src='https://via.placeholder.com/64' alt={el.name} />
            <Sdiv>
                <Sh4>{el.name}</Sh4>
                <span>{el.symbol}</span>
                <div style={{ width: '100%', textAlign: 'left' }}>
                    <span>{el.region}</span>
                    <span>{el.marketOpen} - {el.marketClose} {el.timezone}</span>
                </div>
                <span>{el.matchScore}</span>
                <strong>{el.currency}</strong>
            </Sdiv>
        </Sli>
    )
}

export default ListItem