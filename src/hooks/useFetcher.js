import { useEffect, useState } from 'react'
import axios from 'axios'


function useFetcher(url='https://www.alphavantage.co') {
    const [ keywords, setKeywords ] = useState('inc');
    const [ resource, setResource ] = useState();
    const [ isLoading, setLoading ] = useState(false);
    const [error, setError] = useState('')

    useEffect(() => {
        setLoading(true);
        axios.get(`${url}/query?function=SYMBOL_SEARCH&keywords=${keywords}&apikey=${process.env.REACT_APP_ALPHA_API_KEY}`)
            .then(res => { 
                setLoading(false);
                setResource(res.data)
            })
            .catch(error => { 
                setLoading(false);
                setError(error) 
            })
    }, [ keywords, url ]);

    return { error, isLoading, resource, setKeywords }
}

export default useFetcher