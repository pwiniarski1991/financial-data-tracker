import React from 'react'
import { Sh2 } from './../StyledComponents'
import Form from './../components/Form'


function AddForm(props) {
    return (
        <div>
            <Sh2>Track new company</Sh2>
            <Form {...props} />
        </div>
    )
}

export default AddForm