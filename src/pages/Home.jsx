import React from 'react'
import List from './../components/List'
import ListItem from './../components/ListItem'

function renderItem(item) {
    return <ListItem key={item.symbol} item={item} />
}

function Home({ data }) {
    const items = !data ? null : data.bestMatches.map(el => renderItem(el));

    return (
        <>
            <h2>Companies</h2>
            <List>
                { items }
            </List>
        </>
    )
}

export default Home